variable "customer_policies" {
  type = any
  description = "Managed policies"
}

variable "groups" {
  type = any
  description = "Managed groups"
}

variable "aws_policies" {}

variable "users" {}

variable "roles" {}

locals {
  policies = merge(var.customer_policies, var.aws_policies)
}
