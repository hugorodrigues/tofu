data "aws_iam_policy" "aws" {
  for_each = var.aws_policies
  name = each.key
}

resource "aws_iam_policy" "policy" {
  for_each = var.customer_policies

  name = each.key
  path = try(each.value.path, "/")

  description = try(each.value.description, null)

  tags = try(each.value.tags, null)

  policy = jsonencode(each.value.policy)
}

resource "aws_iam_group" "group" {
  for_each = var.groups

  name = each.key
  path = try(each.value.path, "/")
}

resource "aws_iam_user" "user" {
  for_each = var.users
  name = each.key
  path = try(each.value.path, null)
  tags = try(each.value.tags, null)
}

resource "aws_iam_user_group_membership" "usergroup" {
  for_each = var.users
  user = aws_iam_user.user[each.key].name

  groups = each.value.groups
}

resource "aws_iam_policy_attachment" "attachment" {
  for_each = local.policies
  name = each.value.attachment_name
  users = [for user, data in var.users : user if contains(try(data.policies, []), each.key)]
  roles = [for role, data in var.roles : role if contains(try(data.policies, []), each.key)]
  groups = [for group, data in var.groups : group if contains(try(data.policies, []), each.key)]
  policy_arn = try(data.aws_iam_policy.aws[each.key].arn, aws_iam_policy.policy[each.key].arn)
}
