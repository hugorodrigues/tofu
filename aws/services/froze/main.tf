# SPDX-FileCopyrightText: 2023 Hugo Rodrigues
#
# SPDX-License-Identifier: MIT

# S3 bucket to act as redirect
resource "aws_s3_bucket" "bucket" {
  bucket = "froze"
}

# Lifecycle rules
resource "aws_s3_bucket_lifecycle_configuration" "archive" {
  bucket = aws_s3_bucket.bucket.id

  rule {
    id = "deeparchive"
    status = "Enabled"
    transition {
      days = 7
      storage_class = "DEEP_ARCHIVE"
    }
  }
}
