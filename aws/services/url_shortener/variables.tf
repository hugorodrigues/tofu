# SPDX-FileCopyrightText: 2023 Hugo Rodrigues
#
# SPDX-License-Identifier: MIT

variable "domain" {
  type        = string
  description = "Shortener domain"
}

variable "routing_rules" {
  type        = string
  description = "Rule to be applied on S3 bucket"
}
