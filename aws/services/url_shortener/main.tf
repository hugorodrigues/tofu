# SPDX-FileCopyrightText: 2023 Hugo Rodrigues
#
# SPDX-License-Identifier: MIT

# Route53 Zone
data "aws_route53_zone" "domain" {
  name = var.domain
}

# S3 bucket to act as redirect
resource "aws_s3_bucket" "bucket" {
  bucket = var.domain
}

# Bucket redirect configuration
resource "aws_s3_bucket_website_configuration" "redirects" {
  bucket = aws_s3_bucket.bucket.id

  error_document {
    key = "error.html"
  }

  index_document {
    suffix = "error.html"
  }

  routing_rules = var.routing_rules
}

# Route53 record for S3 bucket
resource "aws_route53_record" "route" {

  zone_id = data.aws_route53_zone.domain.zone_id
  name    = var.domain
  type    = "A"

  alias {
    name                   = aws_s3_bucket_website_configuration.redirects.website_domain
    zone_id                = aws_s3_bucket.bucket.hosted_zone_id
    evaluate_target_health = true
  }
}
