# SPDX-FileCopyrightText: 2023 Hugo Rodrigues
#
# SPDX-License-Identifier: MIT

# Route53 Zone
data "aws_route53_zone" "domain" {
  name = var.domain
}

# S3 policy that allows public read access
data "aws_iam_policy_document" "public_access" {
  statement {
    sid = "PublicReadGetObject"
    principals {
      type = "*"
      identifiers = ["*"]
    }
    actions = [
      "s3:GetObject",
      "s3:ListBucket"
    ]
    effect = "Allow"
    resources = [
      "${aws_s3_bucket.bucket.arn}/*",
      aws_s3_bucket.bucket.arn
    ]
  }
}

# S3 bucket to act as redirect
resource "aws_s3_bucket" "bucket" {
  bucket = "${var.service_name}.${var.domain}"
}

# Bucket redirect configuration
resource "aws_s3_bucket_website_configuration" "website" {
  bucket = aws_s3_bucket.bucket.id

  error_document {
    key = "index.html"
  }

  index_document {
    suffix = "index.html"
  }

}

# Make bucket public
resource "aws_s3_bucket_public_access_block" "access" {
  bucket = aws_s3_bucket.bucket.id

  block_public_acls       = false
  block_public_policy     = false
  ignore_public_acls      = false
  restrict_public_buckets = false
}

# Move packages to ONEZONE_IA class
resource "aws_s3_bucket_lifecycle_configuration" "packages" {
  bucket = aws_s3_bucket.bucket.id

  rule {
    id = "alpine"
    status = "Enabled"
    filter {
      and {
        object_size_greater_than = 131072
        prefix = "alpine/"
      }
    }
    transition {
      days = 30
      storage_class = "ONEZONE_IA"
    }
  }
}

# Apply public read policy to bucket
resource "aws_s3_bucket_policy" "public" {
  bucket = aws_s3_bucket.bucket.id
  policy  = data.aws_iam_policy_document.public_access.json
}

# Route53 record for S3 bucket
resource "aws_route53_record" "route" {

  zone_id = data.aws_route53_zone.domain.zone_id
  name    = aws_s3_bucket.bucket.id
  type    = "A"

  alias {
    name                   = aws_s3_bucket_website_configuration.website.website_domain
    zone_id                = aws_s3_bucket.bucket.hosted_zone_id
    evaluate_target_health = true
  }
}

# Upload web page for browser access
resource "aws_s3_object" "web" {
  for_each = fileset("${path.module}/code", "*")

  bucket = aws_s3_bucket.bucket.id
  key = each.value

  content = replace(replace(file("code/${each.value}"), "@@BUCKET_NAME@@", aws_s3_bucket.bucket.id), "@@BUCKET_URL@@", aws_s3_bucket.bucket.bucket_regional_domain_name)
  storage_class = "STANDARD"
  source_hash = filesha512("code/${each.value}")
  content_type = endswith(each.value, ".html") ? "text/html" : "text/plain"
}
