# SPDX-FileCopyrightText: 2023 Hugo Rodrigues
#
# SPDX-License-Identifier: MIT

variable "domain" {
  type        = string
}
variable "service_name" {
  type        = string
  description = "Name of this service"
}
