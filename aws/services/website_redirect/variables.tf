# SPDX-FileCopyrightText: 2023 Hugo Rodrigues
#
# SPDX-License-Identifier: MIT

variable "domains" {
  type        = list(object({
    domain = string
    zone   = string
  }))
  description = "List of domains to redirect to destination"
}

variable "destination" {
  type        = string
  description = "Destination domain"
}
