# SPDX-FileCopyrightText: 2023 Hugo Rodrigues
#
# SPDX-License-Identifier: MIT

# Route53 Zone
data "aws_route53_zone" "zones" {
  for_each = {for domain in var.domains: domain.zone => domain... }
  name = each.key
}

# S3 buckets with website redirect
resource "aws_s3_bucket" "bucket" {
  for_each = {for domain in var.domains: domain.domain => domain }
  bucket = each.value.domain
}

# Bucket website configuration
resource "aws_s3_bucket_website_configuration" "site" {
  for_each = {for domain in var.domains: domain.domain => domain }
  bucket = aws_s3_bucket.bucket[each.value.domain].id

  redirect_all_requests_to {
    host_name = var.destination
    protocol  = "https"
  }
}

# Route53 record for S3 bucket
resource "aws_route53_record" "records" {
  for_each = {for domain in var.domains: domain.domain => domain }

  zone_id = data.aws_route53_zone.zones[each.value.zone].zone_id
  name    = each.value.domain
  type    = "A"

  alias {
    name                   = aws_s3_bucket_website_configuration.site[each.value.domain].website_domain
    zone_id                = aws_s3_bucket.bucket[each.value.domain].hosted_zone_id
    evaluate_target_health = false
  }
}
