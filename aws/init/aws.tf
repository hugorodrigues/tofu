# SPDX-FileCopyrightText: 2023 Hugo Rodrigues
#
# SPDX-License-Identifier: MIT

terraform {
    required_providers {
        aws = {
            source = "hashicorp/aws"
            version = "~> 5.0"
        }
    }
    backend "s3" {
        bucket = "tfstate.intra.hugorodrigues.xyz"
        key = "init/tf.tfstate"
        encrypt = true
        region = "eu-west-1"
    }
}

provider "aws" {
    region = "eu-west-1"
}
