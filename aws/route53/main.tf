# SPDX-FileCopyrightText: 2023 Hugo Rodrigues
#
# SPDX-License-Identifier: MIT

locals {
  dns_records = flatten([
    for domain_key, domain in var.domains : [
      for record in domain.records : {
        domain = domain_key
        name = record.name
        ttl = record.ttl
        type = record.type
        records = record.records
      }
    ]
  ])
}

# DNS zones
resource "aws_route53_zone" "this" {
  for_each = var.domains

  name = each.key
  comment = ""
}

# Static addresses (not linked to a direct AWS resource)
resource "aws_route53_record" "this" {
  for_each = { for record in local.dns_records : "${aws_route53_zone.this[record.domain].id}_${record.name}_${record.type}" => record }
  zone_id = aws_route53_zone.this[each.value.domain].zone_id
  name    = each.value.name
  ttl     = each.value.ttl
  type    = each.value.type
  records = each.value.records
}

# DNSSEC
resource "aws_kms_key" "this" {
  for_each = var.domains
  customer_master_key_spec = "ECC_NIST_P256"
  key_usage = "SIGN_VERIFY"
}

resource "aws_route53_key_signing_key" "this" {
  for_each = var.domains
  hosted_zone_id             = aws_route53_zone.this[each.key].id
  key_management_service_arn = aws_kms_key.this[each.key].arn
  name                       = replace(each.key, ".", "")
}

resource "aws_route53_hosted_zone_dnssec" "this" {
  for_each = var.domains
  hosted_zone_id = aws_route53_key_signing_key.this[each.key].hosted_zone_id
}
