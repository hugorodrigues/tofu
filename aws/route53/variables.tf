# SPDX-FileCopyrightText: 2023 Hugo Rodrigues
#
# SPDX-License-Identifier: MIT

variable "domains" {
  type = map(object({
    records = list(object({
      name = string
      ttl = number
      type = string
      records = list(string)
    }))
  }))
  description = "Tracked domains"
}
